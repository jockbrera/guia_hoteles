$(function() {
    //$('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#exampleModal').on('show.bs.modal', function(e) {
        console.log("El modal se está mostrando " + new Date().getTime());
        $('#modalBtn').removeClass('btn-outline-success');
        $('#modalBtn').addClass('btn-primary');
        $('#modalBtn').prop('disabled', true);
    });
    $('#exampleModal').on('shown.bs.modal', function(e) {
        console.log("El modal se mostró " + new Date().getTime());
    });
    $('#exampleModal').on('hide.bs.modal', function(e) {
        console.log("El modal se está ocultando " + new Date().getTime());
        $('#modalBtn').removeClass('btn-primary');
        $('#modalBtn').addClass('btn-outline-success');
        $('#modalBtn').prop('disabled', false);
    });
    $('#exampleModal').on('hidden.bs.modal', function(e) {
        console.log("El modal se ocultó " + new Date().getTime());
    });
});